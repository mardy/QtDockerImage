FROM ubuntu:16.04
MAINTAINER mardy@users.sourceforge.net

ARG QT=5.9.1
ARG QTM=5.9
ARG QTSHA=1dc023a2129c5ac7c7bed7ca85cf4c25ff9fa3c83f88d1db202505ac23d4e413
ARG VCS_REF
ARG BUILD_DATE
ARG WORKAREA=/tmp/qt

LABEL org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.name="qt-build" \
      org.label-schema.description="A headless Qt $QTM build environment for Ubuntu" \
      org.label-schema.url="e.g. https://gitlab.com/mardy/QtDockerImage" \
      org.label-schema.vcs-ref="$VCS_REF" \
      org.label-schema.vcs-url="https://gitlab.com/mardy/QtDockerImage" \
      org.label-schema.version="$QT" \
      org.label-schema.schema-version="1.0"

RUN apt-get update -q && \
    DEBIAN_FRONTEND=noninteractive apt-get install -q -y --no-install-recommends \
        build-essential \
        ca-certificates \
        git \
        libasound2 \
        libdbus-1-3 \
        libegl1-mesa \
        libfontconfig1 \
        libgl1-mesa-dev \
        libglib2.0-0 \
        libice6 \
        libnspr4 \
        libnss3 \
        libsm6 \
        libx11-xcb1 \
        libxcomposite1 \
        libxcursor1 \
        libxext6 \
        libxi6 \
        libxrandr2 \
        libxrender1 \
        libxss1 \
        libxtst6 \
        locales \
        openssh-client \
        p7zip \
        xvfb \
    && apt-get clean

ADD qt-installer-noninteractive.qs $WORKAREA/script.qs
#ADD http://download.qt.io/official_releases/qt/${QTM}/${QT}/qt-opensource-linux-x64-${QT}.run $WORKAREA/installer.run
ADD qt-opensource-linux-x64-${QT}.run $WORKAREA/installer.run

RUN echo "${QTSHA}  $WORKAREA/installer.run" | shasum -a 256 -c \
    && chmod +x $WORKAREA/installer.run \
    && xvfb-run $WORKAREA/installer.run -v --script $WORKAREA/script.qs \
     | egrep -v '\[[0-9]+\] Warning: (Unsupported screen format)|((QPainter|QWidget))' \
    && rm -rf $WORKAREA

RUN echo /opt/qt/${QT}/gcc_64/lib > /etc/ld.so.conf.d/qt-${QT}.conf
RUN locale-gen en_US.UTF-8 && DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/qt/${QT}/gcc_64/bin
